package com.scritgroup.roundphoto.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import androidx.core.content.FileProvider;
import java.io.File;
public class CameraFileUtils {

    //Get Uri Of captured Image
    public static Uri getOutputMediaFileUri(Context context, String fileName) {
        File mediaStorageDir = new File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "Camera");
        //If File is not present create directory
        if (!mediaStorageDir.exists()) {
            if (mediaStorageDir.mkdir())
                Log.e("Create Directory", "Main Directory Created : " + mediaStorageDir);
        }
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + fileName);//create image path with system mill and image format
        return FileProvider.getUriForFile(
                context,
            //    "at.co.sktelemed.show2doc.patient.provider", //(use your app signature + ".provider" )
                "com.scritgroup.roundphoto.fileprovider", //(use your app signature + ".provider" )
                mediaFile);
    }

    public static File getFile(Context context, String fileName) {
        File mediaStorageDir = new File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "Camera");
        return new File(mediaStorageDir.getPath() + File.separator
                + fileName);//create image path with system mill and image format
    }
}
