package com.scritgroup.roundphoto.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class BitmapUtils {


    public static Bitmap getBitmapPhotoCamera(Context context, String fileName) {

        int rotation = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(CameraFileUtils.getFile(context, fileName).getAbsolutePath());
            // String orientString = exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotation = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotation = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotation = 270;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String filePhotoOrigPath = CameraFileUtils.getFile(context, fileName).getAbsolutePath();

        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePhotoOrigPath, bounds);

        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap photoFromCamera = BitmapFactory.decodeFile(filePhotoOrigPath, opts);

        //  Log.d("___orig______  ",photoFromCamera.getWidth()+"   "+photoFromCamera.getHeight());
        // переворачиваем, если камера вернула неправильное фото
        if (rotation != 0) {
            Matrix matrix = new Matrix();
            matrix.setRotate(rotation, (float) photoFromCamera.getWidth() / 2, (float) photoFromCamera.getHeight() / 2);
            photoFromCamera = Bitmap.createBitmap(photoFromCamera, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
            //        Log.d("___rot______  ",photoFromCamera.getWidth()+"   "+photoFromCamera.getHeight());
        }

        // уменьшаем, исли фото большого размера с back камеры
        if (photoFromCamera.getWidth() > 1080) {
            Matrix matrix = new Matrix();
            float scale = 1080f / photoFromCamera.getWidth();
            matrix.postScale(scale, scale);
            photoFromCamera = Bitmap.createBitmap(photoFromCamera, 0, 0, photoFromCamera.getWidth(),
                    photoFromCamera.getHeight(), matrix, true);
            //   Log.d("___scal______  ",photoFromCamera.getWidth()+"   "+photoFromCamera.getHeight());

        }
        return photoFromCamera;
    }


    public static void saveBitmapPhotoFile(Bitmap bitmap, File fileName) {

        File file = fileName;
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (null != fos) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
