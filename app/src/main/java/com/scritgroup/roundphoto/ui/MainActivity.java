package com.scritgroup.roundphoto.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Shader;

import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scritgroup.roundphoto.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.scritgroup.roundphoto.utils.*;

import static com.scritgroup.roundphoto.ConstantsApp.*;


public class MainActivity extends AppCompatActivity {


    private ImageView imagePhoto;
    private ImageView imageRound;
    private ImageView imageResult;

    private TextView buttonTakePhoto;
    private TextView buttonCancel;
    private TextView buttonDone;

    private Bitmap photoFromCamera;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imagePhoto = findViewById(R.id.image_photo);
        imageRound = findViewById(R.id.image_roundmask);
        imageResult = findViewById(R.id.image_res);
        buttonTakePhoto = findViewById(R.id.button_takephoto);
        buttonCancel = findViewById(R.id.button_cancel);
        buttonDone = findViewById(R.id.button_done);
    }


    @Override
    protected void onResume() {
        super.onResume();

        buttonTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageResult.setImageBitmap(null);

                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        takePhoto();
                    } else {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }
                } else {
                    takePhoto();
                }
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeResultRoundPhoto();
                imagePhoto.setVisibility(View.INVISIBLE);
                imageRound.setVisibility(View.INVISIBLE);
                imageResult.setVisibility(View.VISIBLE);
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private String fileName;
    private Uri fileUri;

    private void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            fileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            fileUri = CameraFileUtils.getOutputMediaFileUri(this, fileName + ".jpg"); //get fileUri from CameraUtils
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); //Send fileUri with intent
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            photoFromCamera = BitmapUtils.getBitmapPhotoCamera(MainActivity.this, fileName + ".jpg");
            imagePhoto.setImageBitmap(photoFromCamera);
            //       Log.d("_________imagePhoto  ", imagePhoto.getWidth() + "   " + imagePhoto.getHeight());
            drawTransparentRing(); // формирование прозрачного кольца
            imageRound.setVisibility(View.VISIBLE);
            imagePhoto.setVisibility(View.VISIBLE);
        }
    }

    private void makeResultRoundPhoto() {
        float scaleRing = imageRound.getScaleX(); // значение масштаба кольца

        int sizeTemporaryBitmapPhoto = (int) (photoFromCamera.getWidth() * scaleRing);

        // вычисляем смещение x y  (смещение на экране и на фото разные)
        float scaleImageToOriginal = imagePhoto.getWidth() / (float) photoFromCamera.getWidth(); // отношение размера imageView к размеру фото
        //   Log.d("______________", "imagePhoto.getWidth() " + imagePhoto.getWidth() + "  bm.getWidth()   " + photoFromCamera.getWidth());
        int x = (int) ((imageRound.getX() + marginRound * (-1)) / scaleImageToOriginal); // смещение x на оригинале
        int y = (int) ((imageRound.getY() + marginRoundTop * (-1)) / scaleImageToOriginal);// смещение y на оригинале
        //  Log.d("______________", scaleImageToOriginal + "  x   " + x + "y   " + y);

        // обрезанное фото в квадрат по радиусу окружности
        Bitmap resultTemporaryPhoto = Bitmap.createBitmap(photoFromCamera, (int) ((photoFromCamera.getWidth() - sizeTemporaryBitmapPhoto) / 2 + x),
                (int) ((photoFromCamera.getHeight() - sizeTemporaryBitmapPhoto) / 2 + y), sizeTemporaryBitmapPhoto, sizeTemporaryBitmapPhoto);

        // обрезаем фото по кругу
        BitmapShader bmShader = new BitmapShader(resultTemporaryPhoto, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        Paint paintShader = new Paint();
        paintShader.setShader(bmShader);
        Bitmap resultPhoto = Bitmap.createBitmap(sizeTemporaryBitmapPhoto, sizeTemporaryBitmapPhoto, Bitmap.Config.ARGB_8888);
        Canvas canv = new Canvas(resultPhoto);
        canv.drawCircle(sizeTemporaryBitmapPhoto / 2, sizeTemporaryBitmapPhoto / 2, sizeTemporaryBitmapPhoto / 2, paintShader);

        //уменьшаем фото до заданных в ConstantsApp размеров
        resultPhoto = Bitmap.createScaledBitmap(resultPhoto, sizeResultPhoto, sizeResultPhoto, true);
        Log.d("______________", "   imagePhoto.getWidth() " + resultPhoto.getWidth() + "  result.getWidth()   " + resultPhoto.getHeight());

        imageResult.setImageBitmap(resultPhoto);


        BitmapUtils.saveBitmapPhotoFile(resultPhoto, CameraFileUtils.getFile(MainActivity.this, fileName + "_round.png"));
    }


    int marginRound;
    int marginRoundTop;

    private void drawTransparentRing() {

        int roundSise = imagePhoto.getWidth(); // диаметр прозрачного круга = ширине экрана
        int shapeRoundSize = roundSise * 10; // общий размер полупрозрачного круга
        marginRound = (-1) * (shapeRoundSize - roundSise) / 2; // отступ в минус, для исключения обрезки фигуры
        int semiPartRound = marginRound * (-1); // полупрозрачная заливка от внешнего края к центру
        marginRoundTop = marginRound + (imagePhoto.getHeight() - imagePhoto.getWidth()) / 2; // отцентрировать на фото прозрачный круг


        // Shape фигура с прозрачным центром
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.OVAL);
        shape.setColor(Color.TRANSPARENT);
        // shape.setStroke(6480, getResources().getColor(R.color.colorSemiTransparent));
        shape.setStroke(semiPartRound, getResources().getColor(R.color.colorSemiTransparent));


        //  RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(14400, 14400);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(shapeRoundSize, shapeRoundSize);
        // lp.setMargins(-6480, -6170, -6480, -6480);
        lp.setMargins(marginRound, marginRoundTop, marginRound, marginRound);
        imageRound.setLayoutParams(lp);
        // imageRound.setImageDrawable(getResources().getDrawable(R.drawable.shape_round));
        imageRound.setImageDrawable(shape);

// слушатель событий перемещения прозрачного кольца


        x_start = marginRound - imagePhoto.getWidth() / 2;
        x_end = marginRound + imagePhoto.getWidth() / 2;
        y_top = marginRoundTop - imagePhoto.getHeight() / 2;
        y_bottom = marginRoundTop + imagePhoto.getHeight() / 2;


        imageRound.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ImageView view = (ImageView) v;
                viewTransformation(view, event);
                return true;
            }
        });
    }


    //--------------Begin    перемещение/масштабирование кольца
    float[] lastEvent = null;
    //   float d = 0f;
    //   float newRot = 0f;
    private boolean isZoomAndRotate;
    private boolean isOutSide;
    private final int NONE = 0;
    private final int DRAG = 1;
    private final int ZOOM = 2;
    private int mode = NONE;
    private PointF start = new PointF();
    private PointF mid = new PointF();
    float oldDist = 1f;
    private float xCoOrdinate, yCoOrdinate;
    float scale = 1;

    float x_start;
    float x_end;
    float y_top;
    float y_bottom;


    private void viewTransformation(View view, MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                //          Log.d("-----------MotionEvent", "ACTION_DOWN");

                xCoOrdinate = view.getX() - event.getRawX();
                yCoOrdinate = view.getY() - event.getRawY();

                start.set(event.getX(), event.getY());
                isOutSide = false;
                mode = DRAG;
                lastEvent = null;

                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                //           Log.d("-----------MotionEvent", "ACTION_POINTER_DOWN");

                oldDist = spacing(event);
                if (oldDist > 10f) {
                    midPoint(mid, event);
                    mode = ZOOM;
                }

                lastEvent = new float[4];
                lastEvent[0] = event.getX(0);
                lastEvent[1] = event.getX(1);
                lastEvent[2] = event.getY(0);
                lastEvent[3] = event.getY(1);
                //    d = rotation(event);
                break;
            case MotionEvent.ACTION_UP:
                //        Log.d("-----------MotionEvent ", "ACTION_UP");

                isZoomAndRotate = false;

                Log.d("_____________", "getRotation() = " + imageRound.getRotation());
                Log.d("_____________", "getScaleX() = " + imageRound.getScaleX());
                Log.d("_____________", "getX() = " + imageRound.getX() + "getY() = " + imageRound.getY());


                if (mode == DRAG) {
                    float x = event.getX();
                    float y = event.getY();
                }
            case MotionEvent.ACTION_OUTSIDE:
                //       Log.d("-----------MotionEvent ", "ACTION_OUTSIDE");

                isOutSide = true;
                mode = NONE;
                lastEvent = null;
            case MotionEvent.ACTION_POINTER_UP:
                //       Log.d("-----------MotionEvent ", "ACTION_POINTER_UP");

                mode = NONE;
                lastEvent = null;
                break;
            case MotionEvent.ACTION_MOVE:
                //        Log.d("-----------MotionEvent ", "ACTION_MOVE");

                if (!isOutSide) {

                    if (mode == ZOOM && event.getPointerCount() == 2) {
                        float newDist1 = spacing(event);
                        if (newDist1 > 10f) {
                            scale = newDist1 / oldDist * view.getScaleX();
                            if (scale > 1) scale = 1f; // maxScale
                            if (scale < 0.33) scale = 0.33f; // minScale

                            float curentRadius = scale * imagePhoto.getWidth() / 2;
                            if (((imageRound.getX() - curentRadius) < x_start) ||
                                    ((imageRound.getX() + curentRadius) > x_end))
                                imageRound.setX(marginRound);
                            if (((imageRound.getY() - curentRadius) < y_top) ||
                                    ((imageRound.getY() + curentRadius) > y_bottom))
                                imageRound.setY(marginRoundTop);

                            view.setScaleX(scale);
                            view.setScaleY(scale);
                            // Log.d("++++++++ S ", String.format("%.2f", scale) + "");
                            //     textTransformScale.setText(getResources().getString(R.string.scale) + " " + String.format("%.2f", scale));
                        }
                        if (lastEvent != null) {
                            //          newRot = rotation(event);
                            //          view.setRotation((float) (view.getRotation() + (newRot - d)));
                            //    Log.d("++++++++ R ", (view.getRotation() + (newRot - d)) + "");
                            //    textTransformRotation.setText(getResources().getString(R.string.rotation) + " " + (int) (view.getRotation() + (newRot - d)) + getResources().getString(R.string.grad));
                        }
                    }
                    if (mode == DRAG) {
                        isZoomAndRotate = false;
                        //     view.animate().x(event.getRawX() + xCoOrdinate).y(event.getRawY() + yCoOrdinate).setDuration(0).start();
                        float curentRadius = scale * imagePhoto.getWidth() / 2;
                        float xx = event.getRawX() + xCoOrdinate;
                        float yy = event.getRawY() + yCoOrdinate;


                        if ((xx - curentRadius) > x_start && (xx + curentRadius < x_end)) {
                            view.animate().x(xx);
                        }
                        if ((yy - curentRadius) > y_top && (yy + curentRadius) < y_bottom) {
                            view.animate().y(yy);
                        }
                        view.animate().setDuration(0).start();

                        //     Log.d("--------", "xCoOrdinate = " + xx + "   yCoOrdinate = " + yy);

                    }
                }
                break;
        }
    }
/*
    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }*/

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (int) Math.sqrt(x * x + y * y);
    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    //-------END   перемещение/масштабирование кольца


}




